from abc import ABC, abstractmethod
import sounddevice as sd
import soundfile as sf
import numpy as np
import queue
import asyncio
import time
from random import random

# TODO - data queue should be a list of queues, one for each channel
# TODO - add a function to get the number of channels
# TODO - change all data dunction to work with data from a specific channel
# TODO - change class attr to set list of channels to use

def plot_show(data):
    pass
    """Plot show."""
    # import matplotlib.pyplot as plt
    # plt.plot(data)
    # plt.show()

class AbstractAudioData(ABC):
    """Abstract class for audio data, with methods for generating data and manage data queue.
    
    args:
        sample_rate     : sample rate of the audio data
        buffer_size     : size of the buffer
        input_device    : Optional input device
        output_device   : Optional output device
        in_channels     : Optional number of input channels - all by default
        out_channels    : Optional number of output channels - all by default
    
    methods:
        add_data        : add data to the queue
        callback        : callback function for the audio stream
        get_data        : get data from the queue
        get_data_size   : get the size of the queue
        save_data       : save data to a file
        playback_data   : playback data
        close_stream    : close the audio stream
        close           : close the audio stream and the audio interface
    """
    
    def __init__(self,
        sample_rate         : int , 
        buffer_size         : int ,
        input_device        : int | str     = None, 
        output_device       : int | str     = None,
        input_channels      : int | list    = None,
        output_channels     : int | list    = None
        ):
        """ Initialize the audio data object. 
            Create a queue for storing the data, and a counter for counting the number of data.
            save the sample rate and buffer size to self properties.
        args:
            sample_rate : sample rate of the audio data
            buffer_size : size of the buffer
        """
        
        # audio data config
        self.sample_rate = sample_rate
        self.buffer_size = buffer_size
        
        # device config, cill determine which device to use and how call back will be called
        self.input_device = input_device
        self.output_device = output_device
        
        # chnnels config, will determine which channels to use
        channels_if_none = lambda device, input_or_output : list(range(list(sd.query_devices())[device][f"max_{input_or_output}_channels"]))
        channels_clear_input = lambda channels : channels if isinstance(channels, list) else [channels]
        
        self.in_channels = channels_clear_input(input_channels) if input_channels is not None else channels_if_none(self.input_device, "input")
        self.out_channels = channels_clear_input(output_channels) if output_channels is not None else channels_if_none(self.output_device, "output")
        
        # create a queue for storing the data, easyest way of retrieving data from outside this class
        # TODO : outpout channels support
        self.data_queues = [ queue.Queue(maxsize=self.buffer_size) for _ in self.in_channels ]
        
        self.counter = 0
        
        print("Audio stream started")
        
    # change raw input to hertz frequency
    def raw_to_hertz(self, raw):
        """Change raw input to hertz frequency.
        
        args:
            raw : raw input
        returns:
            hertz : hertz frequency
        """
        return raw * self.sample_rate / self.buffer_size
    
    def add_data(self, data, channel):
        """Add data to the queue.
        
        args:
            data    : data to be added to the queue
        """
        def error_file_append(text, error) :
            error_file = open("error.txt", "a")
            add_error = f"{self} \n {error} \n {data} \n"
            print(f"{add_error}-------{text}-------", file=error_file)
            error_file.close()
            # raise sd.CallbackAbort from e
        def add_data_in_file(data, channel, frame):
            """Add data to a file.
            
            args:
                data    : data to be added to the file
                channel : channel of the data
            """
            data_file = open("data.txt", "a")
            print(f"---------DATA from frame {frame} {data.shape}-----------\n\n{data}\n\n", file=data_file)
            data_file.close()
        
        DATA_FROM_CHANNEL = data[:, channel]
        plot_show(DATA_FROM_CHANNEL)

        try:
            # add data to the queue async
            # print(f"adding data to queue {channel}")
            self.counter += 1
            add_data_in_file(DATA_FROM_CHANNEL, channel, self.counter)
            
            # add time to see last time data was added
            if not hasattr(self, "data_time"):
                # create list if it doesn't exist
                self.last_time = time.time()
                self.data_time = []
            else:
                atm = time.time()
                self.data_time.append(atm - self.last_time)
                self.last_time = atm
            
            # if counter is over 1000, print time since last data
            if self.counter % 100 == 0 and len(self.data_time) > 0 :
                mean_time = sum(self.data_time) / len(self.data_time)
                data_mean = np.mean(DATA_FROM_CHANNEL)
                data_std = np.std(DATA_FROM_CHANNEL)
                data_min = np.min(DATA_FROM_CHANNEL)
                data_max = np.max(DATA_FROM_CHANNEL)
                
                print(f"average time between data for audio : {mean_time}, mean : {data_mean}, std : {data_std}, value sent : {DATA_FROM_CHANNEL[0]} min : {data_min}, max : {data_max} - mean-std : {data_mean - data_std}")
                
 
            # # add all the list to the queue
            self.data_queues[channel].put(DATA_FROM_CHANNEL[0], block = False)
            # self.data_queues[channel].put(data[:, channel][1], block = False)
            # for  d in data[:, channel]:
            #     # TODO here or in graph fourrier transform
            #     herz = self.raw_to_hertz(d)
            #     # print(f"adding data to queue {channel} : {herz} hertz - raw : {d}")
            #     self.data_queues[channel].put(d, block = False)
            #     # self.data_queues[channel].put(herz)
            # self.data_queues[channel].put(data)
            # print(f"data on queue for channel : {channel}")
            
        except KeyboardInterrupt as e:
            error_file_append("KeyboardInterrupt", e)
            raise sd.CallbackAbort from e

        except queue.Empty as e:
            error_file_append("Buffer is empty: increase buffersize?", e)
        except queue.Full as e:
            error_file_append("Buffer is full: increase buffersize?", e)
        except Exception as e:
            error_file_append("Unknown error", e)
        
        
    def __status(self, status):
        """Print status.
        
        args:
            status  : status to be printed
        """
        # TODO : manage status with a logger
        # TODO : satus is a bitfield, need to be converted to string ??
        # TODO : status impact on the audio stream and async function
        if status:
            print(status, flush=True)
    
    async def callback(self, indata, frames, time, status):
        """Callback function for the audio stream.
        
        args:
            indata  : input data
            frames  : number of frames
            time    : time
            status  : status of the audio stream
        """
        # TODO : kinda all ?
        self.__status(status)
        
        self.add_data(indata.copy())
   
    
    def callback(self, indata, outdata, frames, time, status):
        """Callback function for the audio stream.
        
        args:
            indata  : input data - numpy array with shape (frames, channels)
            outdata : output data
            frames  : number of frames
            time    : time
            status  : status of the audio stream
        """
        self.__status(status)
        
        # collect output data
        atm_data = indata[:, 0]
        # VALUE_ON_REPEAT = r
        # atm data = same shape with only first value
        atm_data = np.array([random()]*len(atm_data))
        
        # make atm_data same shape as outdata
        atm_data = np.array([atm_data]*2).T
        # print( indata.shape, atm_data.shape, outdata.shape)
        # print(f" indata : {indata.shape} atm_data : {atm_data.shape} outdata : {outdata.shape}")
        outdata[:] = atm_data
        # outdata[:] = indata # TODO - this is probably stereo to stereo, make it work with the audio channels config
        # print(type(indata))
        # print(indata.shape)
        # print(frames)
        # exit()
        # print(indata[0])
        self.add_data(indata, 0 ) # blocking

    def get_data(self, channel):
        """Get data from the queue.
        
        returns:
            data    : data from the queue
        """
        return self.data_queues[channel].get()
    
    def get_data_size(self, channel):
        """Get the size of the queue.
        
        returns:
            size    : size of the queue
        """
        return self.data_queues[channel].qsize()
    
    def save_data(self, filename):
        """Save data to a file, csv and wav file.
        
        args:
            filename    : name of the file
        """
        # get data from the queue
        print("saving data")
        print("has to be redo since don't support multi channels")
        exit()
        data = self.get_data()
        
        # save data to csv file
        np.savetxt(filename + ".csv", data, delimiter=",")
        
        # save data to wav file
        sf.write(filename + ".wav", data, self.sample_rate)
        
    def playback_data(self):
        """Playback data from the queue."""
        # get data from the queue
        print("has to be redo since don't support multi channels")
        exit()
        data = self.get_data()
        
        # playback data
        sd.play(data, self.sample_rate, device=self.output_device)
    
    async def __while_stream_action(self):
        """allow to do something while the stream is running and wait for user pressing enter to stop the stream"""

        # wait for user to press enter, if it's the case, stop the stream
        # otherwise do nothing and wait for the stream to finish
        # print("press enter to stop the stream")
        # a =  input_task = (user_input())
        # print("stopping stream")
        while self.counter < 10000:
            await asyncio.sleep(0.1)
            print(self.counter)
        
        
    async def start_stream(self, *on_stream_action):
        """Start the audio stream."""
        
        # in and out stream print function
        NB_DEVICES = self.__number_of_devices_loaded()
        in_out_print = lambda in_out : print(f"--------audio stream {'started' if in_out else 'finished'} with {NB_DEVICES} devices--------")
        
        in_out_print(True)
        
        # start the audio stream - depending on the device config, callback will be called differently
        if self.input_device is not None and self.output_device is not None:
            # TODO : check args callback and finished_callback can be used together 
            with sd.Stream( device=(self.input_device, self.output_device ), samplerate=self.sample_rate, blocksize=self.buffer_size, callback=self.callback, latency="low"):
                # await asyncio.gather(*on_stream_action, self.__while_stream_action())
                while self.counter < 10000:
                    # print(self.counter)
                    await asyncio.sleep(0.1)
                
                # input_task = asyncio.ensure_future(self.__while_stream_action())
                # await asyncio.gather(input_task)
                
                # if on_stream_action :
                #     await asyncio.gather(*on_stream_action)
                
        elif self.input_device is None and self.output_device is not None:
            with sd.OutputStream(device=self.output_device, samplerate=self.sample_rate, blocksize=self.buffer_size, callback=self.callback):
                pass

                
        elif self.input_device is not None and self.output_device is None:
            with sd.InputStream(device=self.input_device, samplerate=self.sample_rate, blocksize=self.buffer_size, callback=self.callback):
                pass


        else:
            raise Exception("can't start stream if no device specified")
        
        in_out_print(False)
        await asyncio.sleep(0.1)
        



# try:
#     with sd.Stream(device=(args.input_device, args.output_device),
#                    samplerate=args.samplerate, blocksize=args.blocksize,
#                    dtype=args.dtype, latency=args.latency,
#                    channels=args.channels, callback=callback):

    def __number_of_devices_loaded(self):
        """Get the number of devices loaded."""
        is_device = lambda device : 1 if device is not None else 0
        return is_device(self.input_device) + is_device(self.output_device)
    
    def __str__(self) -> str:
        devices_list = list(sd.query_devices())
        device_print = lambda device, devices_list : 'None' if device is None else f"'{devices_list[device]['name']}...' : device raw number {device} , samplerate : {devices_list[device]['default_samplerate']}, max input channels : {devices_list[device]['max_input_channels']}, max output channels : {devices_list[device]['max_output_channels']}"
        all_queue_print = lambda : '\n'.join([f"number of data over max size : {self.get_data_size(queue)}/{self.data_queues[queue].maxsize} in the queue" for queue in self.data_queues])
        return f"\
            AbstractAudioData({self.sample_rate}, {self.buffer_size}) \
            {all_queue_print} \
            current counter : {self.counter} \
            \nwith {self.__number_of_devices_loaded()} devices : \nin<-  {device_print(self.input_device, devices_list)}\nout-> {device_print(self.output_device, devices_list)}"
            
    def get_default_devices():
        """Get default devices."""
        return sd.default.device
    
if __name__ == "__main__":
    
    # print default devices
    default_input_device, default_ouput_device = sd.default.device
    # get devices list
    devices = list(sd.query_devices())
    sample_rate = min(devices[default_input_device]['default_samplerate'], devices[default_ouput_device]['default_samplerate'])
    
    # create an audio data object
    audio_data = AbstractAudioData(
        sample_rate=sample_rate,
        buffer_size=0,
        input_device=default_input_device,
        output_device=default_ouput_device
    )
    print(audio_data)
    # launch stream with async function that just print the number of data in the queue
    
    # function to print the number of data in the queue (send in start_stream)
    # async def print_data_size(audio_data):
    #     while True:
    #         print(audio_data.get_data_size())
    #         await asyncio.sleep(1)

    print("i can get here")
    
    async def main():
        await audio_data.start_stream()
    
    asyncio.run(main())
