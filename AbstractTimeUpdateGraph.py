from abc import ABC, abstractmethod
import asyncio
import queue
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time

class AbstractTimeUpdateGraph(ABC) :
    """Abstract class for real time graph, with methods for manage data queue and printing them on a graph.
    x stands for time, y stands for data
    
    args :
        data_buffer_object : an queue function that returns data
        y_data_lim         : max y value - for graph limits
        axis               : axis object to plot on - if None, will create a new figure
        fig                : figure object to plot on has to be not false if asis is given - if None, will create a new figure
        update_interval    : update interval in ms, in which the graph is updated
        time_kept          : time kept in seconds - how many time will be displayed on the graph
    
    methods :
        create_fig         : create the figure and axis
        start_graph        : start the graph
        async_start_graph  : async start the graph
        update             : update the graph
        async_update       : async update the graph
        get_data           : get data from the queue
        callback           : callback function for an outside data stream (e.g. audio stream)
        close              : close the graph
    """
    
    def __init__(
        self, 
        data_buffer_object : asyncio.Queue | queue.Queue | None = None,  
        y_data_lim         : int | list     = [-1,1], 
        axis                                = None,  
        fig                                 = None, # TODO : change fig to be a plt.figure.Figure or whatever the type is
        update_interval    : int            = 1000,
        time_kept          : int            = 20,    
    ):
        """_summary_

        Args:
            data_buffer_object (asyncio.Queue | queue.Queue | None, optional): the data queue from wich data will be gathered Defaults to None.
            y_data_lim (int, optional): The y lim of the graph, optinal will rescale. Defaults to 20000.
            x_data_lim (int, optional): The x lim of the graph, optinal will rescale. Defaults to 20000.
            axis (plt.axes.Axes, optional): The ax subplot in wich the graphs will be draw, if None a fig will be created. Defaults to None.
            update_interval (int, optional): frequency of update of the graphs (time between each refresh). Defaults to 1000.
            time_kept (int, optional): time jept in second. Defaults to 20.
        """
        
        # graph data config
        self.update_interval = update_interval
        self.time_kept = time_kept # TODO : change to time kept in seconds maybe (1000/update_interval * time_kept)
        
        # graph limits
        self.min_y,self.max_y = (0,y_data_lim) if type(y_data_lim) == int else (y_data_lim[0],y_data_lim[1])
        
        # graph counter
        self.graph_counter = 0
        
        # create a queue for storing the data, easyest way of retrieving data from outside this class
        self.data_queue = data_buffer_object
        
        # create a figure and axis if none is given
        if axis is None :
            self.create_fig()
        else :
            assert fig is not None, "if axis is given, fig must be given too"
            self.ax = axis
            self.fig = axis.get_figure()
        
    
    def create_fig(
        self, 
        title : str = "Real-time Graph",
        xlabel : str = "Time",
        ylabel : str = "Datas",
    ) :
        """Create the figure and axis. If already created, will change title and labels and axis limits.
        
        Args:
            title (str, optional): title of the graph. Defaults to "Real-time Graph".
            xlabel (str, optional): xlabel of the graph. Defaults to "Time".
            ylabel (str, optional): ylabel of the graph. Defaults to "Datas".
        """
        
        # Set up the figure and axis
        
        # if self as no attribute fig, create a new figure
        if not hasattr(self, "fig") or self.fig is None :
            self.fig, self.ax = plt.subplots()
        
        # clear the axis and create a new line
        self.x, self.y = [], []
        self.line, = self.ax.plot(self.x, self.y)
        
        # Adjust the y-axis limits as needed
        self.ax.set_ylim(self.min_y, self.max_y)
        # self.ax.set_xlim(self.min_x, self.max_x)
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)
        self.ax.set_title(title)
    

    def start_graph(self) :
        """Start the graph. And will start the animation.
        """
        self.ani = FuncAnimation(
            self.fig,                   # figure to plot on
            self.update,                # update function
            # blit=True,                # blit is faster but can cause artifacts, see animation documentation ?
            interval=self.update_interval, 
            save_count=self.time_kept,  # save_count is the number of frames to cache
            # repeat=False                # repeat is False so that the animation stops when the data is over
        )
        plt.show()
        
        return self.ani
    
    async def async_start_graph(self) :
        """Start the graph. And will start the animation - but asycnronously hehe.
        """
        print("starting animation - async")
        self.ani = FuncAnimation(
            self.fig,                   # figure to plot on
            self.async_update,          # update function
            # blit=True,                # blit is faster but can cause artifacts, see animation documentation ?
            interval=self.update_interval, 
            save_count=self.time_kept,  # save_count is the number of frames to cache
            # repeat=False                # repeat is False so that the animation stops when the data is over
        )
        print("animation started")
        # self.ani.event_source.start()
        # print("ploted")
        # plt.show() # TODO omg omg omg what is that
        # Display the figure without blocking

        # print("showed")            
        
        # return self.ani
    
    async def plot_show(self) :
        """Show the graph.
        """
        plt.show(block=False)
        # while self has not been cut (i.e. is_cut isn't an attribute or is False)
        # TODO : change to while not self.is_cut if problems
        while True : #not hasattr(self, "is_cut") or not self.is_cut :
            # print("plot show")
            plt.pause(self.update_interval)
            # await asyncio.sleep(0.1)
        print("plot show finished")
        self.close()
    
    def update(self, frame):
        """Update the graph. Will get data from the queue, and add it to the graph.
        
        Args:
            frame (int): frame number
        """
        # Add new data point
        self.x.append(self.graph_counter)
        self.graph_counter += 1
        
        # try getting data from the queue
        # print("getting data")
        try :
            in_data = self.get_data()
            data = in_data if in_data is not None else 0
                        
            # add time to see last time data was added
            if not hasattr(self, "data_time"):
                # create list if it doesn't exist
                self.last_time = time.time()
                self.data_time = []
            else:
                atm = time.time()
                self.data_time.append(atm - self.last_time)
                self.last_time = atm
            
            # if counter is over 1000, print time since last data
            if self.graph_counter % 100 == 0 and len(self.data_time) > 0 :
                mean_time = sum(self.data_time) / len(self.data_time)
                print(f"average time between data for graphs : {mean_time} - queue size : {self.data_queue.qsize()}/{self.data_queue.maxsize}")

        except KeyboardInterrupt:
            print('\nInterrupted by user')
            self.close()
 
        except Exception as e :
            print("error getting data")
            print(str(e))
            print(type(e))
            data = None
            
        # if data is not None cut the animation
        if data is None or (hasattr(self, "is_cut") and self.is_cut):
            if not hasattr(self, "is_cut") :
                print("cutting graph")
                self.is_cut = True
                # pause the animation
                # self.ani.event_source.pause()
                self.y.append(0)
                self.line.set_data(self.x, self.y)
        
        else : 
            self.y.append(data)

            # Keep a certain number of data points (e.g., last 50)
            if len( self.x ) > self.time_kept :
                self.x.pop(0)
                self.y.pop(0)

            # Update the plot
            # print("updating plot")
            self.line.set_data(self.x, self.y)
            
            self.ax.relim()
            self.ax.autoscale_view()
        
    def async_update(self, frame):
        """Update the graph. Will get data from the queue, and add it to the graph.
        
        Args:
            frame (int): frame number
        """
        # print("updating graph")
        # plt.pause(self.update_interval/1000)
        try :
            self.update(frame)
        except KeyboardInterrupt:
            print('\nInterrupted by user')
            self.close()
        except Exception as e :
            print("error updating graph")
    
    def get_data(self):
        """Get data from the queue. Will get data from the queue, and add it to the graph.
        
        Returns:
            data : data from the queue
        """
        # import random
        # val = self.max_y * random.uniform(0,1)
        # print(val)
        # return val
        def error_file_append(text, error) :
            error_file = open("error.txt", "a")
            add_error = f"{self} \n {error} \n {self.data_queue} \n"
            print(f"{add_error}-------{text}-------", file=error_file)
            error_file.close()
            # raise mlp error
            # raise Exception("AbstractTimeUpdateGraph error with queue")
        
        if self.data_queue is not None :
            # try getting data from the queue
            try :
                # print("getting data from queue")
                # TODO timeout just added
                data_from_queue = self.data_queue.get(timeout=self.update_interval/1000, block=False)
                # print("got data from queue")
                return data_from_queue
            except queue.Empty as e :
                error_file_append("queue empty", e)
                return None
            except Exception as e :
                error_file_append("queue error", e)
                return None
        else :
            return None
        
    def callback(self, data):
        """Callback function for an outside data stream (e.g. audio stream).
        
        Args:
            data : data from the outside stream
        """
        pass
    
    def close(self) :
        """Stop the animation and close the figure after user input.
        """
        print("closing graph")
        self.ani.event_source.stop()
        print("animation closed, press enter to close the figure")
        input()
        plt.close(self.fig)   
    
    def __str__(self) :
        return f"AbstractTimeUpdateGraph : {self.graph_counter} --options-- updates interval = {self.update_interval} time kept = {self.time_kept}, max y = {self.max_y} , data queue status = {self.data_queue.qsize()}/{self.data_queue.maxsize}"

 
if __name__ == "__main__":
    import random
    # create a queue
    q = queue.Queue()
    # add data to the queue
    for i in range(35) :
        q.put(20000*random.uniform(0,1))
    
    # create a graph
    GRAPH = AbstractTimeUpdateGraph(data_buffer_object=q)
    print(GRAPH )
    # start the graph
    # ani = GRAPH.start_graph()
    # print("graph started")
    
    # an async function for non blocking testing
    async def test() :
        interval = GRAPH.update_interval/1000
        print("starting test - interval = ", interval)
        for i in range(15) :
            print("test : ", i)
            await asyncio.sleep(interval)
        print("test finished")
    
    # start the graph asyncronously
    async def main() :
        test_task = asyncio.create_task(test())
        graph_task = asyncio.create_task(GRAPH.async_start_graph())
        
        graph_show = asyncio.create_task(GRAPH.plot_show())
        
        await asyncio.gather( graph_task, test_task, graph_show, return_exceptions=True)
        
        
        print("both finished")
        
        # GRAPH.close()
        # print("graph closed")
    
    asyncio.run(main())