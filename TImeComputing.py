import time


# make a class for time calculation 
class TimeCalculation:
    """This class is used to calculate the time in betwen the two events on asynchrnous mode but should work for synchronous mode as well
    """
    
    def __init__(self, name, print_every_x_iterations = 1000, print_every_x_seconds = 60):
        
        self.start_time = time.time()
        self.end_time = self.start_time
        self.time_difference = 0
        self.time_difference_list = []
        
        # for between x itte
        self.last_print_time = self.start_time
        self.last_print_iteration = 0
        
        self.x_iterations = print_every_x_iterations
        self.x_seconds = print_every_x_seconds
        self.name = name
    
    def iteration(self):
        """This function is used to calculate the time difference between two events
        """
        atm = time.time()
        self.time_difference = atm - self.end_time
        self.end_time = atm
        self.time_difference_list.append(self.time_difference)
        
        # print if needed
        self.print_every_x_iterations()
        self.print_every_x_seconds(atm)
    
    def print_every_x_iterations(self):
        """This function is used to print the time difference between two events after x iterations
        """
        if len(self.time_difference_list) % self.x_iterations == 0:
            average_time = sum(self.time_difference_list)/len(self.time_difference_list)
            average_since_last_print = sum(self.time_difference_list[-self.x_iterations:])/self.x_iterations
            last_iteration = self.time_difference_list[-1]
            print(f"Average time for {self.name} iterations average | global : {average_time} ; recent : {average_since_last_print} ; last : {last_iteration}")
    
    def print_every_x_seconds(self, atm):
        """This function is used to print the time difference between two events after x seconds
        """
        if atm - self.last_print_time > self.x_seconds:
            average_time = sum(self.time_difference_list)/len(self.time_difference_list)
            average_since_last_print = sum(self.time_difference_list[-self.x_iterations:])/self.x_iterations
            last_iteration = self.time_difference_list[-1]
            number_of_iterations = len(self.time_difference_list) - self.last_print_iteration
            print(f"Average time for {self.name} ; number of itteration {number_of_iterations} ; iterations average | global : {average_time} ; recent : {average_since_last_print} ; last : {last_iteration}")
            self.last_print_time = time.time()
            self.last_print_iteration = len(self.time_difference_list)
            
    
if __name__ == "__main__":
    # test the class
    time_calculation = TimeCalculation("test")
    for i in range(10000):
        # make some computation
        q = [i**2 for i in range(100000)]
        time_calculation.iteration()
    
    print("end")
        