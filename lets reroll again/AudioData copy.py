import sounddevice as sd
import soundfile as sf
import queue
import threading
import sys

class AudioData:
    """Class for storing audio data."""

    def __init__(self, buffer_size=1024, sample_rate=44100):
        
        self.stream = sd.InputStream(callback=self.callback)
        self.stream.start()
        
        self.buffersize = buffer_size
        self.sample_rate = sample_rate
        
        
        self.queue = queue.Queue(maxsize=self.buffersize)
        
        print("Audio stream started")
        
        
    def callback(self, indata, frames, time, status):
        print('dead')
        exit()
        if status:
            print(status, flush=True)
        self.x.append(self.graph_counter)
        self.graph_counter += 1
        self.y.append(indata.mean())  # Use the mean value of the input data
        if len(self.x) > self.time_kept:
            self.x.pop(0)
            self.y.pop(0)
        self.line.set_data(self.x, self.y)
        self.ax.relim()
        self.ax.autoscale_view()
    
    def play(self):
        sd.play(self.y, self.stream)
    
    
class AudioDataFile(AudioData):
    """load data from a wav file"""
    def __init__(self, filename, device, blocksize=1024, *args, **kwargs):
        self.filename = filename
        self.device = device
        
        super().__init__()
        
        self.blocksize = blocksize
        # assert block size is int
        assert type(self.blocksize) == int, f"blocksize must be an int, but is {type(self.blocksize)} with value {self.blocksize}"
        
        self.event = threading.Event()
        
    
    def load(self):
        """load the data from the file"""
        #load the file
        with sf.SoundFile(self.filename) as f:
            # each buffer is x second long (x = buffersize / samplerate)
            for _ in range(self.buffersize):
                data = f.buffer_read(self.buffersize, dtype='float32')
                # is it the end of the file?
                if not self.data:
                    break
                self.queue.put_nowait(data)
                
            # this is for playing the file on output device
            print(f"sample rate {self.sample_rate}, block size {self.blocksize} device {self.device}, channel {f.channels}")
            self.stream = sd.OutputStream(
                samplerate=self.sample_rate, blocksize=self.blocksize,
                device=self.device, channels=f.channels,
                callback=self.callback, finished_callback=self.event.set
            )
            print("playback started")
            
            
            with self.stream:
                timeout = self.blocksize * self.buffersize / self.sample_rate
                while len(self.data):
                    print("putting data in queue")
                    print(f)
                    print(self.data)
                    self.data = f.read(self.blocksize)
                    print("data read")
                    print(self.data)
                    self.queue.put(self.data, timeout=timeout)
                self.event.wait()  # Wait until playback is finished
                print("playback finished")
                
                
    def callback(self, outdata, frames, time, status):
        
        assert frames == self.blocksize
        
        if status.output_underflow:
            print('Output underflow: increase blocksize?', file=sys.stderr)
            raise sd.CallbackAbort
        
        assert not status
        
        try:
            self.data = self.queue.get_nowait()
        except queue.Empty as e:
            print('Buffer is empty: increase buffersize?', file=sys.stderr)
            raise sd.CallbackAbort from e
        
        print("data in callback")
        print(self.data)
        
        if len(self.data) < len(outdata):
            # Explicitly reshape data to (128, 1)
            outdata[:len(self.data)] = self.data.reshape(-1, 1)
            outdata[len(self.data):].fill(0)
            raise sd.CallbackStop
        else:
            # Explicitly reshape data to (128, 1)
            outdata[:] = self.data.reshape(-1, 1)
    
    def play(self):
        """"""
    def __str__(self) -> str:
        return f"\
            AudioDataFile({self.filename}) \
            with  {self.buffersize} buffersize \
            and {self.sample_rate} sample rate \
            \n buffer is {self.buffersize}\
        "
        
    
if __name__ == "__main__":
    # list all audio devices
    print(sd.query_devices())
    device_index = 7
    # try loadind 440hz_test.wav
    la_test = AudioDataFile("440hz_test.wav", device_index)
    print(la_test)
    la_test.load()
        
