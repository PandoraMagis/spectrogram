import sounddevice as sd
import soundfile as sf
import queue
import threading
import sys

class AudioData:
    """Class for storing audio data."""

    def __init__(self, buffer_size=1024, sample_rate=44100, callback_object = None):
        
        
        self.buffersize = buffer_size
        self.sample_rate = sample_rate
        
        self.counter = 0
        
        self.callback_object = callback_object
        print(f"callback object is {self.callback_object}")
        
        self.queue = queue.Queue(maxsize=self.buffersize)
        
        print("Audio stream started")

    
    
class AudioDataFile(AudioData):
    """load data from a wav file"""
    def __init__(self, filename, device, blocksize=1024, *args, **kwargs):
        self.filename = filename
        self.device = device
        
        super().__init__(*args, **kwargs)
        
        self.blocksize = blocksize
        # assert block size is int
        assert type(self.blocksize) == int, f"blocksize must be an int, but is {type(self.blocksize)} with value {self.blocksize}"
        
        self.event = threading.Event()
        
    
    def load(self):
        try:
            with sf.SoundFile(self.filename) as f:
                for _ in range(self.buffersize):
                    data = f.read(self.blocksize)
                    if not len(data):
                        break
                    self.queue.put_nowait(data)  # Pre-fill queue
                stream = sd.OutputStream(
                    samplerate=f.samplerate, blocksize=self.blocksize,
                    device=self.device, channels=f.channels,
                    callback=self.callback, finished_callback=self.event.set)
                with stream:
                    timeout = self.blocksize * self.buffersize / f.samplerate
                    while len(data):
                        data = f.read(self.blocksize)
                        self.queue.put(data, timeout=timeout)
                    self.event.wait()  # Wait until playback is finished
        except KeyboardInterrupt:
            exit('\nInterrupted by user')
        except queue.Full:
            # A timeout occurred, i.e. there was an error in the callback
            exit(1)
        except Exception as e:
            exit(type(e).__name__ + ': ' + str(e))
                
                
    def callback(self, outdata, frames, time, status):
        
        try:
            assert frames == self.blocksize
        except AssertionError:
            print(f"AssertionError: frames ({frames}) do not match expected blocksize ({self.blocksize}) on callback {self.counter}", file=sys.stderr)
            
            
        if status.output_underflow:
            print('Output underflow: increase blocksize?', file=sys.stderr)
            raise sd.CallbackAbort
        assert not status
        try:
            data = self.queue.get_nowait()
            self.read = data
        except queue.Empty as e:
            print('Buffer is empty: increase buffersize?', file=sys.stderr)
            raise sd.CallbackAbort from e
        if len(data) < len(outdata):
            # Explicitly reshape data to (128, 1)
            outdata[:len(data)] = data.reshape(-1, 1)
            outdata[len(data):].fill(0)
            raise sd.CallbackStop
        else:
            # Explicitly reshape data to (128, 1)
            outdata[:] = data.reshape(-1, 1)
        
        self.counter += 1
         # Call the method in the callback_object with the processed data
        if self.callback_object is not None:
            print("callback")
            self.callback_object.add_audio_data(self.read)

    
    def play(self):
        """"""
    def __str__(self) -> str:
        return f"\
            AudioDataFile({self.filename}) \
            with  {self.buffersize} buffersize \
            and {self.sample_rate} sample rate \
            \n buffer is {self.buffersize}\
        "
        
    
if __name__ == "__main__":
    # list all audio devices
    print(sd.query_devices())
    device_index = 7
    # try loadind 440hz_test.wav
    la_test = AudioDataFile("440hz_test.wav", device_index)
    print(la_test)
    la_test.load()
        
