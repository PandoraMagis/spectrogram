from AbstractAudioData import AbstractAudioData

class AudioDataTest(AbstractAudioData):
    """class who just generate random data and add it to the queue"""
    
    def __init__(self, *args, **kwargs):
        """ Initialize the audio data object. 
            Create a queue for storing the data, and a counter for counting the number of data.
            save the sample rate and buffer size to self properties.
        args:
            sample_rate : sample rate of the audio data
            buffer_size : size of the buffer
        """
        super().__init__( *args, **kwargs)
            
    def generate_data(self):
        """Generate random data"""
        import random
        return random.randint(0, 20000)

    def add_data(self, data):
        return super().add_data(data)
    
    def __str__(self) -> str:
        print(f"AudioDataTest object with sample rate {self.sample_rate} and buffer size {self.buffer_size}")
        return super().__str__()

    

if __name__ == "__main__":
    test = AudioDataTest(44100, 1024)
    print(test)
    test.playback_data()
    