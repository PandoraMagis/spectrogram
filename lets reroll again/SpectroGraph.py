import matplotlib.pyplot as plt
import numpy as np
import queue
from TimeUpdateGraph import TimeUpdateGraph

class SpectroGraph(TimeUpdateGraph):
    
    # init with suplementary args possible for spectrogram
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.graph_queue = queue.Queue(maxsize=int(self.time_kept*(1000/self.update_interval)))
        # show lengh of queue
        print(f"graph queue has size {self.graph_queue.maxsize}, ie for {self.time_kept} seconds")
        self.stared = False

    def create_fig(self) :
        super().create_fig()
        pass

    def update(self, frame):
        # Add new data point
        self.x.append(self.graph_counter)
        self.graph_counter += 1
        
        ###### CARE THIS IS THE SUPER#####
        self.y.append(self.get_audio_data())

        # Keep a certain number of data points (e.g., last 50)
        if len( self.x ) > self.time_kept :
            self.x.pop(0)
            self.y.pop(0)

        # Update the plot
        self.line.set_data(self.x, self.y)
        
        self.ax.relim()
        self.ax.autoscale_view()
    
    def generate_data(self):
        print("woooow the code should very not got here")
        exit("badaboum")
    
    def add_audio_data(self, audio_data):
        self.graph_queue.put(audio_data)
        print(f"added audio data to queue, queue size is now {self.graph_queue.qsize()}")

            
    def get_audio_data(self):
        # if queue is empty, return 0
        if self.graph_queue.empty():
            return 0
        d = self.graph_queue.get()
        self.graph_queue.task_done()
        return d