import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation



class TimeUpdateGraph:
    
    def __init__(
        self, 
        y_data_lim = 20000, 
        update_interval = 1000, 
        time_kept = 20
    ):
        
        self.min_y = 0
        self.max_y = y_data_lim
        
        # graph and data config
        self.update_interval = update_interval
        self.time_kept = time_kept
        
        
        self.graph_counter = 0
        self.create_fig()
        
    
    
    def create_fig(self) :
        # Set up the figure and axis
        self.fig, self.ax = plt.subplots()
        self.x, self.y = [], []
        self.line, = self.ax.plot(self.x, self.y)
        
        # Adjust the y-axis limits as needed
        self.ax.set_ylim(0, self.max_y)   
        self.ax.set_xlabel('Time')
        self.ax.set_ylabel('Amplitude')
        self.ax.set_title('Real-time Graph')



    # Function to update the plot in real-time
    def update(self, frame):
        # Add new data point
        self.x.append(self.graph_counter)
        self.graph_counter += 1
        
        
        self.y.append(self.generate_data())

        # Keep a certain number of data points (e.g., last 50)
        if len( self.x ) > self.time_kept :
            self.x.pop(0)
            self.y.pop(0)

        # Update the plot
        self.line.set_data(self.x, self.y)
        
        self.ax.relim()
        self.ax.autoscale_view()
        
    
    def start(self):
        # Set up plot to call animate() function periodically
        # interval = 1000ms
        self.ani = FuncAnimation(
            self.fig,
            self.update,
            interval=self.update_interval,
            save_count=self.time_kept
        )
        
        plt.show()
        #         # Set up the animation
        # ani = FuncAnimation(
        #     self.fig, 
        #     self.update, 
        #     blit=False, 
        #     interval=self.update_interval # Update every second
        # )
        
        return self.ani
    
    def generate_data(self):
        import random
        return self.max_y * random.uniform(0, 1)


if __name__ == "__main__":
    STD_GRPH_TEST = TimeUpdateGraph()
    
    STD_GRPH_TEST.start()