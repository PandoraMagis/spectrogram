import queue
import numpy as np
import sounddevice as sd
import soundfile as sf
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Parameters
sample_rate = 44100
duration = 5
channels = 1

# Initialize Queue
audio_queue = queue.Queue()

# Audio Callback Function
def audio_callback(indata, frames, time, status):
    if status:
        print(status, flush=True)
    audio_queue.put(indata.copy())

# Initialize Sound Device
sd.callback(audio_callback, channels=channels)
stream = sd.OutputStream(samplerate=sample_rate, channels=channels)

# Animation Function
def update_plot(frame):
    data = audio_queue.get()
    # Process data and update the plot
    # Example: plt.plot(data)

    # Playback the audio
    stream.write(data)

# Initialize Matplotlib Animation
fig, ax = plt.subplots()
ani = FuncAnimation(fig, update_plot, blit=False)
plt.show()

# Start the Audio Stream and Run the Animation
with stream:
    with sd.InputStream(samplerate=sample_rate, channels=channels):
        sd.sleep(int(duration * 1000))
