# imports - base or modules
import asyncio
import queue
# import my submodules
from AbstractAudioData import AbstractAudioData
from AbstractTimeUpdateGraph import AbstractTimeUpdateGraph

##### Global variables
CHUNK = 1024  # Number of frames per buffer
# FORMAT = pyaudio.paInt16  # Audio format (16-bit PCM)
CHANNELS = 1  # Number of audio channels (1 for mono, 2 for stereo)
RATE = 44100  # Sampling rate (samples per second)
DEVICE_INDEX = 7  # Device index


##### def main
async def main(
        AudioData   : AbstractAudioData, 
        Graphs      : AbstractTimeUpdateGraph
    ) :
    """Main entry point of the program
    run async function for both audio loading and plotting
    """
    
    print("starting main function")
    
    try :
        # starting audio stream asyncronously
        audio_task = asyncio.create_task(AudioData.start_stream())
        
        # await asyncio.sleep(0.05)
        # starting animating asyncronously
        anim_task = asyncio.create_task(Graphs.async_start_graph())
        
        # starting plotting asyncronously
        plot_task = asyncio.create_task(Graphs.plot_show())
        
        
        # wait for both to finish
        print("waiting for both to finish")
        #trying just awaiting audio

        await asyncio.gather( audio_task, anim_task, plot_task, return_exceptions=True)
        print("both finished")
    except KeyboardInterrupt :
        print("KeyboardInterrupt")
        print("stopping audio stream")
        AudioData.stop_stream()
        print("stopping animation")
        Graphs.stop_graph()
        print("stopping plot")
        Graphs.stop_plot()
        print("all stopped")
    
    
##### run main
if __name__ == "__main__":
    # rm error file
    import os
    if os.path.exists("error.txt"):
        os.remove("error.txt")
    # load i/o Audio
    print("loading audio interface with default devices")
    
    def_in, def_out = AbstractAudioData.get_default_devices()
    
    AUDIO = AbstractAudioData(44100, 1024, input_device=def_in, output_device=def_out, input_channels=1)
    print("audio interface loaded")
    print(AUDIO)

    # load interface
    print("loading graphs interface")
    queue = queue.Queue()
    #fill queue with data
    import random
    for i in range(100):
        queue.put( 20000*random.uniform(0,1) )
    # get one data point
    data = queue.get()
    GRAPHS = AbstractTimeUpdateGraph(AUDIO.data_queues[0], update_interval=1, time_kept= 500, y_data_lim=[-1,1])
    # GRAPHS = AbstractTimeUpdateGraph(queue)
    
    # run main
    asyncio.run(main(AUDIO, GRAPHS))