import sounddevice as sd
import speech_recognition as s_r

def getMicrophoneDevicesInfo() :
    ##  Browsing default devices
    defaultDevices = sd.default.device
    defaultDevices = [ i  for i in sd.query_devices() if i["index"] in defaultDevices ]

    ##  Filtering default devices from standar query to get complet object
    spekear = [ i for i in defaultDevices if i["max_output_channels"] > 0 ][0]
    mic = [ i for i in defaultDevices if i["max_input_channels"] > 0 ][0]

    print(f"Scanned to {mic['name'].strip()}")
    return mic

def getMicrophone() :
    ##  checking microphone and connecting it
    micobject = s_r.Microphone(device_index=getMicrophoneDevicesInfo()["index"])
    return micobject

if __name__ == "__main__":
    print(getMicrophone())