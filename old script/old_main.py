import matplotlib.pyplot as plt
import numpy as np
import speech_recognition as s_r

## my file imports :
from findMicrophone import getMicrophone

r = s_r.Recognizer()
mic = getMicrophone()
with mic as source:
    print("Say now!!!!")
    r.adjust_for_ambient_noise(source) #reduce noise
    try :
        audio = r.listen(source)
    except Exception as e :
        print("dead by timeout")
print(audio)
# print(r.adjust_for_ambient_noise(source))
# print(r.recognize_google(audio))
exit()
## base Objects
frequencies = np.arange(0, 300)
samplingFrequency = 1000
samples, signal = np.empty([0]), np.empty([0])

# Start Value of the sample
start   = 1
# Stop Value of the sample
stop    = samplingFrequency+1

# Stop Value of the sample

stop    = samplingFrequency+1
for frequency in frequencies:
    sub1 = np.arange(start, stop, 1)
    # Signal - Sine wave with varying frequency + Noise
    sub2 = np.sin(2*np.pi*sub1*frequency*1/samplingFrequency)+np.random.randn(len(sub1))
    samples = np.append(samples, sub1)
    signal  = np.append(signal, sub2)
    start   = stop+1
    stop    = start+samplingFrequency

#signal
plt.subplot(3,1,1)
plt.plot(samples, signal)
plt.xlabel('Sample')
plt.ylabel('Amplitude')

print(f"{len(samples)} samples")
print(f"{len(signal)} signal")
print(f"{len(frequencies)} frequencies")

#spectrogram
plt.subplot(3,1,3)
plt.plot(frequency, signal[300:][1])
plt.xlabel('Frequency')


plt.show()